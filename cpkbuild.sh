#!/bin/sh
## Copyright (c) 2024 by mobinmob  <mobinmob@disroot.org
##This program is free software: you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free 
## Software Foundation, either version 2.0 of the License, or (at your option)
## any later version.
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
## FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
## You should have received a copy of the GNU General Public License along with
## this program. If not, see <https://www.gnu.org/licenses/>.

# Default dirs and general data for the script operation
script_dir="$(dirname "$(realpath "$0")")"
builddir="$script_dir/rootfs"
pkgdir="$script_dir/packages"
workdir="$builddir/var/cache/scratchpkg/work/"
rootfstarball="$script_dir/venomlinux-rootfs-sysv-x86_64.tar.xz"
# Default values for variables that control prog operation
[ "$op_keeproot" != "1" ] && op_keeproot="" 
[ "$op_keepsource" != "1" ] && op_keepsource=""
[ "$op_buildintmpfs" != "1" ] && op_buildintmpfs=""
[ "$op_nativebuild" != "1" ] && op_buildintmpfs=""
# Colour ouput
green=$(tput setaf 2)
red=$(tput setaf 1)
yellow=$(tput setaf 3)
reset_colour=$(tput sgr0)
#log to a file for earch invocation
log_file="$script_dir"/"$(basename "$0")_$(date +"%Y%m%d_%T")".log

error() {
	printf "(!!) Error: %40s\n" "${red} $* ${reset_colour}" && exit 1
}

warn() {
	printf "(~~) Warning: %40s\n" "${yellow} $* ${reset_colour}"
}

info() {
	printf "(~>) Info: %40s\n" "${green} $* ${reset_colour}" 
}

chroot_run() {
	chroot "$builddir" "$@"
}

usage() {
	cat <<-EOH
	Usage: $(basename "$0") [options]

	*c*lean *p*ac*k*age *build*
	Build one or more packages for Venom Linux, starting always from a pristine chroot.
	
	OPTIONS
	 -p <package1,package2>	Build a list of (comma-seperated) packages
	 -n	Reuse sources and packages if run in a Venom Linux system.
	 -r	Keep extracted rootfs for other builds or inspection.
	 -s	Keep source packages for future builds.
	 -f	Build using a tmpfs for work directory.
	 -h	Display this usage information.
	EOH
}
extract_tarball() {
	## Fetches, checks and extracts rootfs tarball
	info Fetching and extracting the rootfs tarball.
	$fetchcmd https://nc.abetech.es/index.php/s/CPBw2WjkD9MfHaD/download/venomlinux-rootfs-sysv-x86_64.tar.xz.sha512sum || \
		error No network, exiting.
	[ -e "$rootfstarball" ] ||  \
		$fetchcmd https://nc.abetech.es/index.php/s/9c6TbRg6xryA8yY/download//venomlinux-rootfs-sysv-x86_64.tar.xz

	sha512sum -c "$rootfstarball".sha512sum 
	if ! sha512sum -c "$rootfstarball".sha512sum ; then
		rm -f "$rootfstarball"
		rm -f "$rootfstarball".sha512sum
		error Corrupted download or wrong checksum, rootfs removed. Please run the script again.
	exit 1
	fi 
	if ! [ -e "$builddir"/ports ]; then
		mkdir -p "$builddir" || error Cannot create builddir!
		info Extracting rootfs tarball to "$builddir"..
		tar -xf "$rootfstarball" -C "$builddir"/ || error Cannot extract rootfs! 
	else
		info rootfs/ exists, will not extract the tarball.
	fi
}
prepare_dirs() {
	info Creating necessary directories.
	# Create /packages and /sources dirs in builddir and possibly $script_dir
	mkdir -p "$builddir"/packages
	mkdir -p "$builddir"/sources
	export SOURCE_DIR="/sources"
	[ ! -d "$pkgdir" ] && mkdir -p "$pkgdir"
	if [ -n "$op_buildintmpfs" ]; then
		[ -d "$workdir" ] && mount -t tmpfs -o noatime tmp_workdir "$workdir"
		info Building in tmpfs...
	fi
	
# Source the os=release file and use the ID value to determine if the host is a venom system.
# shellcheck disable=1091
	# Backup original configuration files
	for i in scratchpkg.repo scratchpkg.alias scratchpkg.conf; do
			cp "$builddir"/etc/"$i" "$builddir"/etc/"$i".original; done
	# shellcheck disable=SC1090
	# shellcheck disable=SC1091
	[ -e /etc/os-release ] && . /etc/os-release
	if [ -n "$op_nativebuild" ] && [ "$ID" != "venom" ]; then
		info You are not on a Venom linux system, proceeding without using packages and sources from the host!
	elif [ -n "$op_nativebuild" ] && [ "$ID" = "venom" ]; then
		mount --rbind /var/cache/scratchpkg/sources "$builddir"/sources
		export pkgdir=/var/cache/scratchpkg/packages
		# Backup rootfs conf files and copy host files
		for i in scratchpkg.repo scratchpkg.alias scratchpkg.conf; do
			\cp -r "$i" "$builddir"/etc/; done
	fi

	if [ -n "$op_keepsource" ] && [ -z "$op_nativebuild" ]; then
		mkdir -p "$script_dir"/sources
		mount --bind "$script_dir"/sources "$builddir"/sources
	fi
	mount --bind "$pkgdir" "$builddir"/packages
	export PACKAGE_DIR="/packages"
	printf "%s\n" "export PACKAGE_DIR=$PACKAGE_DIR" >> "$builddir"/etc/scratchpkg.conf
	printf "%s\n" "export SOURCE_DIR=$SOURCE_DIR" >>  "$builddir"/etc/scratchpkg.conf
 }

curl_or_wget() {
	# Find fetch tool to use.
	if [ "$(which curl)" ]; then 
		fetchcmd="curl -OL"
	elif [ "$(which wget)" ]; then 
		fetchcmd="wget"
	else 
		error No suitable download program found, you need wget or curl! 
	exit 1
	fi
}

prepare_chroot() {
	for i in dev proc sys; do 
		mount --rbind /"$i" "$builddir"/"$i"
		mount --make-rslave "$builddir"/"$i";done
	cp /etc/resolv.conf "$builddir"/etc/
}

umount_pseudofs() {
	for i in dev proc sys; do
		umount -R "$builddir"/"$i" || unmount_issue=1; done
}

cleanup() {
	# Unmount external directories
	umount -R "$builddir"/packages
	mountpoint -q "$builddir"/sources && umount -R "$builddir"/sources
	mountpoint -q "$workdir" && umount -R "$workdir"
	# Unmount psedo-fs, do not remove them from $builddir if imposible to unmount.
	umount_pseudofs
	[ -n "$unmount_issue" ] && \
		dirs_to_clean="opt srv tmp mnt root boot sbin etc run bin usr home lib"
	# Clean chroot if it will remain.
	[  "$op_keeproot" = "1" ] && chroot_run pkgbase -y
	# Restore scratch configuration 
	if [ -n "$op_keeproot" ] && [ -f "$builddir"/etc/scratchpkg.conf.backup ]; then
		for c in conf alias repo; do
			if [ -e scratchpkg."$c".original ]; then
				mv "$builddir"/etc/scratchpkg."$c".original "$builddir"/etc/scratchpkg."$c"
			fi
		done
	fi
	# Create a checksum file for all packages in $pkgdir.
	cd "$pkgdir" && sha512sum ./* > packages.sha512sums
	if [ -z "$op_keeproot" ]; then
		info Removing the builddir...
		if [ -n "$dirs_to_clean" ]; then
			for i in $dirs_to_clean; do rm -rf "${builddir:?}"/"$i";done
			rm -f "$builddir"/ports
		else rm -rf "$builddir" ||  error Could not remove builddir, exiting...
		fi
	else
		info Keeping the builddir for inspection or other builds.	
	fi
}

scratch_build_packages() {
	package_list="$(echo "$packages" | tr ',' ' ')"
	[ -z "$package_list" ] && warn Not building packages, just upgrading rootfs.
	for i in $package_list; do 
		chroot_run scratch install "$i" -y 
		chroot_run pkgbase -y
	done
}

main() {
	info Full comand given is [cpkbuild.sh "$showargs"].
	[ "$(id -u)" -ne 0 ] && error You need to be root to run this program!
	cd "$script_dir" || exit
	curl_or_wget
	extract_tarball
	prepare_chroot
	prepare_dirs 
	chroot_run scratch sync -y
	# shellcheck disable=2181
	[ "$?" -ne 0 ] && warn Repos did not sync, see above..
	chroot_run scratch upgrade scratchpkg -y
	# shellcheck disable=2181
	[ "$?" -ne 0 ] && warn scratchpkg did not self-upgrade, see above...
	# shellcheck disable=2181
	[ "$?" -ne 0 ] && warn scratch did not perform a system upgrade, see above...
	scratch_build_packages
	chroot_run scratch sysup -y
	# shellcheck disable=2181
	[ "$?" -ne 0 ] && warn scratch did not perform a system upgrade, see above...
	chroot_run revdep -r -y
	cleanup
}

trap "cleanup && exit 1" INT TERM

while getopts "p:nrsfh" OPTION; do
	case "${OPTION}" in
		p) packages="$OPTARG";;
		n) op_nativebuild=1;;
		r) op_keeproot=1;;
		s) op_keepsource=1;;
		f) op_buildintmpfs=1;;
		h) usage ; exit 0;;
		*) usage ; exit 0;;
	esac
done
# Capture all the arguments given
showargs="$(printf "%s" "$*")"
shift $((OPTIND - 1))
[ "$OPTIND" -eq 1 ] && usage && exit
main "$@" 2>&1 | tee -- "$log_file"
