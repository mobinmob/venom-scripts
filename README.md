## Scripts for working with Venom Linux

### cpkbuild.sh: clean package build

Build one or more packages for Venom Linux, starting always from a clean chroot.

**Usage:**
```shell
cpkbuild.sh [options]
```

| OPTIONS | Info |
| ---- | ---- |
| -p <package1,package2> | Build a list of (comma-seperated) packages |
| -n | Reuse sources and packages if run in a Venom Linux system. |
| -r | Keep extracted rootfs for other builds or inspection. |
| -s | Keep source packages for future builds. |
| -f | Build using a tmpfs for work directory. |
| -h | Display this usage information. |
				
To use, clone the repository, enter the repo directory and run the script. The script should work 
on any linux distribution that provides coreutils,busybox or toybox. It runs as root, requires an
internet connection and outputs venom linux packages in the `package` subdirectory.

Status: alpha, ``p|r|s|f|h`` options are tested, `-n` needs more work.


### pkbuild-container.sh: a reduced version of cpkbuild.sh for use in a venom container

**Usage:**
```shell
pkbuild-container.sh [options]
```

| OPTIONS | Info |
| ---- | ---- |
| -p <package1,package2> | Build a list of (comma-separated) packages |
| -h | Display this usage information. |
